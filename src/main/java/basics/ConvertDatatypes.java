package basics;

public class ConvertDatatypes {
    
    public static void main(String[] args) {
        // double to int
        double myDouble = 5.9;
        int myConvertedDouble = (int) myDouble;

        System.out.println("Original: " + myDouble);
        System.out.println("Converted: " + myConvertedDouble);

        // int to double
		int myInt = 10;
        double myConvertedInt = (double) myInt;

        System.out.println("\nOriginal: " + myInt);
        System.out.println("Converted: " + myConvertedInt);

        // int to string
		String myConvertedString = Integer.toString(myInt);

        System.out.println("\nOriginal: " + myInt);
        System.out.println("Converted: " + myConvertedString);

        // string to int
        String myString = "kake";
        int mySecondConvertedString = Integer.parseInt(myString);

        //System.out.println("\nOriginal: " + myString);
        //System.out.println("Converted: " + mySecondConvertedString);
    } 
}
