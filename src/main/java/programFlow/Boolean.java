package programFlow;

public class Boolean {

	public static void main(String[] args) {
		boolean boolean1 = false;
		if (boolean1) {
			System.out.println("Boolean1 is true");
		}
		else if (!boolean1) {
			System.out.println("Boolean1 is false");
		}
		else {
			System.out.println("Boolean1 is neither true nor false");
		}
		
		
		
		
		
		int myInt = 100;
		if (myInt > 100) {
			System.out.println("This number is big");
		}
		else if (myInt < 100) {
			System.out.println("This number is small");
		}
		else {
			System.out.println("This number is equal to 100");
		}
		
		
		
		
		
		boolean boolean2 = false;
		if (boolean1 && boolean2) {
			System.out.println("Expression 1 is true");
		}
		else if (boolean1 || boolean2) {
			System.out.println("Expression 2 is true");
		}
		else {
			System.out.println("Non are true");
		}
	}
}
